
### 1. Create network
```
docker network create students-app
```

### 2. Create database instance
```
docker run -d --network students-app --network-alias mysql -v students-mysql-data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=secret -e MYSQL_DATABASE=<your_group_id> mysql:8.0
```

### 3. Setup database
```
docker exec -it <mysql-container-id> mysql -u root -p
```
Give permissions for the `root` user
```
mysql> ALTER USER 'root' IDENTIFIED WITH mysql_native_password BY 'secret';
mysql> flush privileges;
```
Create table
```
CREATE TABLE Students (
    ID int NOT NULL AUTO_INCREMENT,
    FirstName varchar(255) NOT NULL,
    MiddleName varchar(255) NOT NULL,
    LastName varchar(255) NOT NULL,
    Email varchar(255) NOT NULL,
    PRIMARY KEY (ID)
);
```
Fill database with data of your group. For example:
```
INSERT INTO Students (FirstName, MiddleName, LastName, Email) VALUES ('Vasyl', 'Vasyliovych', 'Vasyltsiv', 'vasyl.v.vasyltsiv@lpnu.ua');
```

### 4. Check MySQL is available
Run container with network utilities to check if databse is available via the docker network
```
docker run -it --network students-app nicolaka/netshoot
```
Check if `mysql` is resolved
```
host mysql
```

Now run the same docker image without `--network` option
```
docker run -it nicolaka/netshoot
```
Check if `mysql` is resolved via `host` or `dig` commands
```
host mysql
```

### 5. Build `python-simple-app`
There is python application in directory called `python-simple-app`. Go to it and run command
```
docker build -t python-simple-app --build-arg GROUP_NAME=<your_group_id> .
```
Check if docker image has been successfuly built
```
docker images
```

### 6. Run application using docker
```
docker run -d -p 5001:5001 python-simple-app
```
Now open http://127.0.0.1:5001/hello/student in your web browser or run the command in your terminal
```
curl http://127.0.0.1:5001/hello/student
```

